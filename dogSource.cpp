//Source file containing the other misc functions
//Dustin - Tuesday 4-6:45

#include <iostream>
#include "dogHeader.h"

using std::cout;
using std::cin;
using std::endl;
using std::cerr;


//function that gains the input height and checks for validity
float inputWeight()
{
	float inWeight = 0.0;
	cout << "What weight is your dog?" << endl;
	cin >> inWeight;
	cout << endl;

	while (inWeight < 0.0)
	{
		cout << "Please specify a valid weight (non-negative)." << endl;
		cin >> inWeight;
		cout << endl;
	}
	return inWeight;
}

//function that gains the input height and checks for validity
float inputHeight()
{
	float inHeight = 0;
	cout << "How tall is your dog?" << endl;
	cin >> inHeight;
	cout << endl;

	while (inHeight < 0)
	{
		cout << "Please specify a valid height." << endl;
		cin >> inHeight;
		cout << endl;
	}
	return inHeight;
}

//function to gain the input time
int inputTime()
{
	int inTime = 0;
	cout << "Please specify a time of day (0-23)." << endl;
	cin >> inTime;
	cout << endl;
	return inTime;
}

//help commands from command h
void commandHelp()
{
	cout << "\'h\' - The help command. (Shows this set.)" << endl
	<< "\'q\' - The quit command. (Quits the application.)" << endl
	<< "\'d\' - The display command. (Displays the dog's attributes.)" << endl 
	<< "\'e\' - The eat command. (Make the dog eat some food.)" << endl
	<< "\'b\' - The bark command. (Make the dog bark, or make noise.)" << endl
	<< "\'s\' - The sleep command. (Make the dog sleep and burn calories.)" << endl
	<< "\'w\' - The walk command. (Take the lovely dog for a walk!)" << endl
	<< "\'c\' - The chase command. (Make the dog chase someone.)" << endl
	<< "\'p\' - The play command. (Play with the dog, he loves fetch!)" << endl;
}

//the main input loop for those that wish to attempt killing the dog
void commandLoop()
{
	Dog aDog; //Initializes the class

	aDog.setWeight(inputWeight()); //gets the weight
	aDog.setHeight(inputHeight()); //gets the height

	int timeOfDay = inputTime(); //gets the time
	int counter = 0;

	while (counter == 0)
	{
		char inputCommand;
		cout << "What command would you like to initiate? (type \'h\' for help)" << endl;
		cin >> inputCommand;
		cout << endl;

		if (inputCommand == 'h')
		{
			commandHelp();
		}

		if (inputCommand == 'q')
		{
			counter = 1;
		}

		if (inputCommand == 'd')
		{
			cout << "The time of day that you specified is " << timeOfDay << " hours (military time)." << endl;
			cout << "This dog's name is: " << aDog.getName() << endl;
			cout << "This dog weighs: " << aDog.getWeight() << " lbs." << endl;
		 	cout << "This dog is only " << aDog.getHeight() << " inches tall." <<  endl;
		 	cout << "This dog is of the breed: " << aDog.getBreed() << endl;
		 	cout << "This dog is a colorful " << aDog.getColor() << endl;
		}

		if (inputCommand == 'e')
		{
			float inputFood = 0;
			cout << "How much would you like to feed your dog? (in lbs.)" << endl;
			cin >> inputFood;
			cout << endl;

			aDog.eat(inputFood);
		}

		if (inputCommand == 'b')
		{
			aDog.sound(timeOfDay);
		}

		if (inputCommand == 's')
		{
			aDog.sleep(timeOfDay);
		}

		if (inputCommand == 'w')
		{
			aDog.walk(timeOfDay);
		}
		
		if (inputCommand == 'c')
		{
			aDog.chase(timeOfDay);
		}
		
		if (inputCommand == 'p')
		{
			aDog.playful(timeOfDay);
		}
	}
}

//function for recalculating weight based on behaviors
float Dog::calcNewWeight()
{
	float newWeight = getWeight() - 0.5;
	if (newWeight <= 0)
	{
		cerr << "You should feed your dog more food so it doesn't die from being overworked!" << endl << endl;
		return newWeight;
	}
	else
	{
		return newWeight;
	}
}

float Dog::getWeight() const
{
	return mWeight;
}

void Dog::setWeight(float weight)
{
	if (weight > 0)
	{
		mWeight = weight;
	}
}

float Dog::getHeight() const
{
	return mHeight;
}

void Dog::setHeight(float height)
{
	mHeight = height;
}

string Dog::getName() const
{
	return mName;
}

void Dog::setName(string name)
{
	mName = name;
}

string Dog::getBreed() const
{
	return mBreed;
}

void Dog::setBreed(string breed)
{
	mBreed = breed;
}

string Dog::getColor() const
{
	return mColor;
}

void Dog::setColor(string color)
{
	mColor = color;
}

Dog::Dog ()
{
	setName("Fido.");
	setBreed("Australian Shepard.");
	setColor("marbled rust and white.");
}

void Dog::sound(int time)
{	
	float newWeight = calcNewWeight(); 
	setWeight(newWeight); //updates the weight from what happened in the calcNewWeight() function
	if (newWeight > 0) //double checks that the weight didn't go under 0 with the calcNewWeight() function
	{
		if (time <= 12) 
		{
			cerr << "This dog barks too much during the day! So you won't have a problem getting him to bark more. Your dog lost 0.5 lbs and now weighs " << getWeight() << " lbs." << endl << endl;
		}
		else
		{
			cout << "This dog doesn't bark at all during the day. But if you wave a toy in front of him he'll bark a lot! Your dog lost 0.5 lbs and now weighs " << getWeight() << " lbs." << endl << endl;
		}
	}
}

void Dog::sleep(int time)
{
	float newWeight = calcNewWeight(); 
	setWeight(newWeight);
	if (newWeight > 0)  
	{
		if (time >=14)
		{
			cout << "This dog sleeps normally! But you bored the dog to sleep. Your dog burned 0.5 lbs worth of calories and now weighs " << getWeight() << " lbs." << endl << endl;
		}
		else 
		{
			cerr << "This dog doesn't like to sleep. But tranquilizer darts never harmed anyone.... Your dog burned 0.5 lbs worth of calories and now weighs " << getWeight() << " lbs." << endl << endl;
		}
	}

}

void Dog::walk(int time)
{
	float newWeight = calcNewWeight(); 
	setWeight(newWeight);
	if (newWeight > 0) 
	{
		if (time > 6 && time < 20)
		{
			cerr << "This dog needs to be walked more than usual! But that's okay, since it needs to lose weight anyway right? Your dog lost 0.5 lbs and now weighs " << getWeight() << " lbs." << endl << endl;
		}
		else 
		{
			cout << "This dog doesn't need to be walked. But you can drag it out with the teasing of treats! Your dog lost 0.5 lbs and now weighs " << getWeight() << " lbs." << endl << endl;
		}
	}
}

void Dog::chase(int time)
{
	float newWeight = calcNewWeight(); 
	setWeight(newWeight);
	if (newWeight > 0) 
	{
		if (time < 19)
		{
			cout << "This dog chases the mailman less than other dogs! Just hand that mailman a beggin strip so you can shed some weight from that dog! Your dog lost 0.5 lbs and now weighs " << getWeight() << " lbs." << endl << endl;
		}
		else 
		{
			cerr << "This dog will chase anyone it sees, so just let him loose! Your dog lost 0.5 lbs and now weighs " << getWeight() << " lbs." << endl << endl;
		}
	}
}

void Dog::playful(int time)
{	
	float newWeight = calcNewWeight(); 
	setWeight(newWeight);
	if (newWeight > 0) 
	{
		if (time > 4 && time < 22)
		{
			cout << "This dog enjoys to play often! So just throw that ball out so you can play fetch! Your dog lost 0.5 lbs and now weighs " << getWeight() << " lbs." << endl << endl;
		}
		else
		{
			cerr << "This dog doesn't play very much. But after a few throws the dog decides to appease you and play along. Your dog lost 0.5 lbs and now weighs " << getWeight() << " lbs." << endl << endl;
		}
	}
}

void Dog::eat(float inputFood)
{
	float tenPercentDogWeight = getWeight() * 0.1;
	float addDogHeight = getHeight() + 0.1;

	if (inputFood >= getWeight() * 0.05 && inputFood <= tenPercentDogWeight) 	   //checks for the food percentage being between 5% and 10%
	{
		float dogWeight = getWeight() + inputFood;
		cout << "You feed the dog the right amount of food, it gains some weight and grows a little!" << endl << endl;
		setWeight(dogWeight);
		setHeight(addDogHeight);
	}

	if (inputFood > tenPercentDogWeight)		 //checks for food percentage for making the poor thing sick
	{
		float dogWeight = getWeight() + tenPercentDogWeight;
	    cerr << "You fed the dog too much food and the poor thing got sick, but it still grew 0.1 inches and gained a bit of weight" << endl << endl;
		setWeight(dogWeight);
		setHeight(addDogHeight);
	}

	if (inputFood < getWeight() * 0.05) 		//if the food fed to the dog is under 5% do this
	{
		float dogWeight = getWeight() + inputFood;
		cout << "You feed the dog, and it gains a little weight." << endl << endl;
		setWeight(dogWeight);
	}
}