//Header file containing the Dog class
//Dustin - Tuesday 4-6:45

#include <string>
using std::string;

class Dog
{
public:
	explicit Dog();
	
	float getHeight() const;
	void setHeight(float height);

	float getWeight() const;
	void setWeight(float weight);

	string getName() const;
	void setName(string name);

	string getBreed() const;
	void setBreed(string breed);

	string getColor() const;
	void setColor(string color);

	void sound(int time);
	void sleep(int time);
	void walk(int time);
	void chase(int time);
	void playful(int time);

	void eat(float inputFood);

private:

	string mName;
	float mHeight;
	string mBreed;
	string mColor;
	float mWeight;
	float calcNewWeight();
};

	float inputWeight();
	float inputHeight();
	int inputTime();
	void commandHelp();
	void commandLoop();
