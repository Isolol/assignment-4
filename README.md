assignment-4
============

School Assignment 4

Your Dog class is now required to have a behavior called "eat". The "eat" behavior is going to take a float as an argument. The float represents the amount of food the dog eats.  In addition:
 If the food amount is greater than 10% of the dogs current weight, print a message about the dog being sick from overeating.  
 Add the amount of food ( up to a max of  10% of the dogs current weight) to the current weight. Your dog is growing.
 If the amount of food is over 5% of the dogs current wieght, then the dog should grow by 0.1 in height.
Your main program is going to accept input in a loop. All input should be in lower case. 
If the input is a 'h', your program will display a "help" message that shows the different commands dog object will accept.
If the input is a 'd', your program will display all of the dogs attributes.
If the input is a 'q', your program loop will terminate.
Each of your dogs behaviors should have a command letter that lets me trigger it.
Each of your dogs behaviors should reduce its weight by .5.
Your dogs weight should never go below 0!
